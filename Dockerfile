FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install software-properties-common -y
RUN apt-add-repository -y ppa:ansible/ansible
RUN apt-get update
RUN apt install ansible -y
